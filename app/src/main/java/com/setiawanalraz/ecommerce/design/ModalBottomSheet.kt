package com.setiawanalraz.ecommerce.design

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.setiawanalraz.ecommerce.databinding.BottomSheetBinding

class ModalBottomSheet : BottomSheetDialogFragment() {
    private var _binding: BottomSheetBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnReset.setOnClickListener {
            resetFilter()
        }
    }

    private fun resetFilter() {
        binding.apply {
            chipReview.isChecked = false
            chipSale.isChecked = false
            chipLowestPrice.isChecked = false
            chipHighestPrice.isChecked = false

            chipApple.isChecked = false
            chipAsus.isChecked = false
            chipDell.isChecked = false
            chipLenovo.isChecked = false

            tietLowestPrice.text = null
            tietHighestPrice.text = null
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}