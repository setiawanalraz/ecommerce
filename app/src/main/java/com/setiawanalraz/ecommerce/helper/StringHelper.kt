package com.setiawanalraz.ecommerce.helper

import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan

fun String.setSubstringColor(listTarget: List<String>, color: Int): Spannable {
    return SpannableString(this).apply {
        listTarget.forEach { target ->
            val start = this.indexOf(target)
            val end = start + target.length
            setSpan(
                ForegroundColorSpan(color),
                start,
                end,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}