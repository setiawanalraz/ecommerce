package com.setiawanalraz.ecommerce.helper

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SharedPreferencesHelper @Inject constructor(
    @ApplicationContext context: Context
) {

    private val sharedPreferences =
        context.getSharedPreferences("AppPreferences", Context.MODE_PRIVATE)

    fun isFirstTimeLaunch(): Boolean {
        return sharedPreferences.getBoolean("isFirstTimeLaunch", true)
    }

    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        sharedPreferences.edit().putBoolean("isFirstTimeLaunch", isFirstTime).apply()
    }

    fun saveAccessToken(accessToken: String) {
        sharedPreferences.edit().putString("accessToken", accessToken).apply()
    }

    fun getAccessToken(): String? {
        return sharedPreferences.getString("accessToken", null)
    }
}