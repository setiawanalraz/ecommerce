package com.setiawanalraz.ecommerce.helper

import java.text.NumberFormat
import java.util.Locale

fun formatPrice(price: Int): String {
    val locale = Locale("id", "ID")
    val numberFormat = NumberFormat.getCurrencyInstance(locale)

    // Set minimum and maximum fraction digits to zero
    numberFormat.minimumFractionDigits = 0
    numberFormat.maximumFractionDigits = 0

    return numberFormat.format(price.toDouble()).replace("IDR", "").replace(",", "")
}