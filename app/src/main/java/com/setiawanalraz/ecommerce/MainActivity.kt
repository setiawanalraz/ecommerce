package com.setiawanalraz.ecommerce

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val navHostFragment: NavHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.nhf_main) as NavHostFragment
    }
    private val navController: NavController by lazy { navHostFragment.navController }

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun toDetailProduct(productId: String) {
        val bundle = Bundle()
        bundle.putString("productId", productId)
        val navController = findNavController(R.id.nhf_main)
        navController.navigate(R.id.main_to_product, bundle)
    }

    fun toReviewProduct(productId: String) {
        val bundle = Bundle()
        bundle.putString("productId", productId)
        val navController = findNavController(R.id.nhf_main)
        navController.navigate(R.id.main_to_review, bundle)
    }

    fun logout() {
        navController.navigate(R.id.main_to_prelogin)
    }
}