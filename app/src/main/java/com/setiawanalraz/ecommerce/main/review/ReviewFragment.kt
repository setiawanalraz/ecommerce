package com.setiawanalraz.ecommerce.main.review
//
//import android.os.Bundle
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Toast
//import androidx.fragment.app.Fragment
//import androidx.fragment.app.viewModels
//import androidx.lifecycle.lifecycleScope
//import androidx.navigation.fragment.findNavController
//import androidx.recyclerview.widget.LinearLayoutManager
//import com.setiawanalraz.ecommerce.databinding.FragmentReviewBinding
//import com.setiawanalraz.ecommerce.helper.SharedPreferencesHelper
//import dagger.hilt.android.AndroidEntryPoint
//import kotlinx.coroutines.launch
//import javax.inject.Inject
//
//@AndroidEntryPoint
//class ReviewFragment : Fragment() {
//
//    @Inject
//    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
//
//    private var _binding: FragmentReviewBinding? = null
//    private val binding get() = _binding!!
//
//    private val viewModel: ReviewViewModel by viewModels()
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        _binding = FragmentReviewBinding.inflate(inflater, container, false)
//        return binding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        val productId = arguments?.getString("productId")
//        val authToken = sharedPreferencesHelper.getAccessToken().toString()
//        viewLifecycleOwner.lifecycleScope.launch {
//            viewModel.isLoading.value = true
//            try {
//                val productReviewResponse =
//                    viewModel.fetchProductReview(authToken, productId.toString())
//                viewModel.isLoading.value = false
//
//                if (productReviewResponse != null) {
//                    val recyclerView = binding.reviewRecyclerView
//                    recyclerView.layoutManager = LinearLayoutManager(context)
//
//                    val adapter = ReviewAdapter(productReviewResponse)
//                    recyclerView.adapter = adapter
//                } else {
//                    binding.progressBar.visibility = View.VISIBLE
//                }
//            } catch (e: Exception) {
//                Toast.makeText(requireContext(), e.message.toString(), Toast.LENGTH_SHORT).show()
//            }
//        }
//
//        viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
//            if (isLoading) {
//                binding.progressBar.visibility = View.VISIBLE
//            } else {
//                binding.progressBar.visibility = View.GONE
//            }
//        }
//
//        binding.topAppBar.setOnClickListener {
//            findNavController().navigateUp()
//        }
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//        _binding = null
//    }
//}

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.setiawanalraz.ecommerce.databinding.FragmentReviewBinding
import com.setiawanalraz.ecommerce.helper.SharedPreferencesHelper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ReviewFragment : Fragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    private var _binding: FragmentReviewBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ReviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentReviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeData()
    }

    private fun initView() {
        binding.topAppBar.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun observeData() {
        val productId = arguments?.getString("productId")
        val authToken = sharedPreferencesHelper.getAccessToken().toString()
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.isLoading.value = true
            try {
                val productReviewResponse =
                    viewModel.fetchProductReview(authToken, productId.toString())
                viewModel.isLoading.value = false

                if (productReviewResponse != null) {
                    val recyclerView = binding.reviewRecyclerView
                    recyclerView.layoutManager = LinearLayoutManager(context)

                    val adapter = ReviewAdapter(productReviewResponse)
                    recyclerView.adapter = adapter
                } else {
                    binding.progressBar.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                Toast.makeText(requireContext(), e.message.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}