package com.setiawanalraz.ecommerce.main.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.setiawanalraz.ecommerce.database.cart.CartDao
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.helper.formatPrice
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(private val cartDao: CartDao) : ViewModel() {

    val cartItems: Flow<List<CartEntity>> = cartDao.getAllCartItems()

    private val _totalPrice = MutableLiveData<String>()
    val totalPrice: LiveData<String> get() = _totalPrice

    fun updateCartItem(cartItem: CartEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            cartDao.updateCart(cartItem)
        }
    }

    fun updateCartItemSelection(productId: String, isSelected: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            cartDao.updateCartItemSelection(productId, isSelected)
        }
    }

    fun deleteCartItem(cartItem: CartEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            cartDao.deleteCart(cartItem)
        }
    }

    fun deleteSelectedItems(selectedItems: List<CartEntity>) {
        viewModelScope.launch(Dispatchers.IO) {
            cartDao.deleteCartItems(selectedItems)
        }
    }

    fun calculateTotalPrice(selectedItems: List<CartEntity>): String {
        val total = selectedItems.sumOf { (it.productPrice?.times(it.quantity)) ?: 0 }
        val formattedPrice = formatPrice(total)
        _totalPrice.postValue(formattedPrice)
        return formattedPrice
    }
}