package com.setiawanalraz.ecommerce.main.wishlisht

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.database.wishlist.WishlistEntity
import com.setiawanalraz.ecommerce.databinding.WishlistItemBinding
import com.setiawanalraz.ecommerce.helper.formatPrice

class WishlistAdapter(
    private val onDeleteClickListener: (WishlistEntity) -> Unit,
    private val onAddClickListener: (WishlistEntity) -> Unit
) : RecyclerView.Adapter<WishlistAdapter.WishlistViewHolder>() {

    private var wishlistItems: List<WishlistEntity> = emptyList()

    @SuppressLint("NotifyDataSetChanged")
    fun setWishlistItems(items: List<WishlistEntity>) {
        wishlistItems = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val binding = WishlistItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return WishlistViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        val currentItem = wishlistItems[position]
        holder.bind(currentItem)
    }

    override fun getItemCount(): Int {
        return wishlistItems.size
    }

    inner class WishlistViewHolder(private val binding: WishlistItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: WishlistEntity) {
            binding.apply {
                tvProductName.text = item.productName
                tvProductPrice.text = item.productPrice?.let { formatPrice(it) }
                tvStoreName.text = item.store
                tvProductRating.text = item.productRating.toString()

                Glide.with(itemView.context)
                    .load(item.image)
                    .placeholder(R.drawable.default_product_image)
                    .into(ivProduct)

                // Set a click listener for the delete button
                btnDelete.setOnClickListener {
                    onDeleteClickListener.invoke(item)
                }

                btnAddToCart.setOnClickListener {
                    onAddClickListener.invoke(item)
                }
            }
        }
    }
}