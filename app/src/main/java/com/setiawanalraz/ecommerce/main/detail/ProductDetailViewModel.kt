package com.setiawanalraz.ecommerce.main.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.setiawanalraz.ecommerce.data.ProductDetailData
import com.setiawanalraz.ecommerce.data.ProductDetailResponse
import com.setiawanalraz.ecommerce.database.AppDatabase
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.network.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {

    @Inject
    lateinit var appDatabase: AppDatabase

    private val _productDetail = MutableLiveData<ProductDetailResponse>()
    val productDetail: LiveData<ProductDetailResponse> = _productDetail

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    var isLoading = MutableLiveData<Boolean>()

    suspend fun fetchProductDetail(authToken: String, productId: String): ProductDetailData? {
        val response = apiService.getProductDetail("Bearer $authToken", productId)
        if (response.isSuccessful) {
            return response.body()?.data
        }
        return null
    }

    suspend fun insertCartItem(cartEntity: CartEntity) {
        appDatabase.cartDao().insertCart(cartEntity)
    }
}