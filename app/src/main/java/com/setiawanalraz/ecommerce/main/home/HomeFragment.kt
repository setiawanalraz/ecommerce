package com.setiawanalraz.ecommerce.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.setiawanalraz.ecommerce.MainActivity
import com.setiawanalraz.ecommerce.database.AppDatabase
import com.setiawanalraz.ecommerce.databinding.FragmentHomeBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnLogout.setOnClickListener {
            // Use a CoroutineScope to run the database operation on a background thread
            CoroutineScope(Dispatchers.IO).launch {
                val database = AppDatabase.getInstance(requireContext())
                database.cartDao().deleteAllCartItems()
                withContext(Dispatchers.Main) {
                    (requireActivity() as MainActivity).logout()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}