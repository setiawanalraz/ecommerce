package com.setiawanalraz.ecommerce.main.cart

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.databinding.CartItemBinding
import com.setiawanalraz.ecommerce.helper.formatPrice

class CartAdapter(private val listener: CartItemClickListener) :
    ListAdapter<CartEntity, CartAdapter.CartViewHolder>(CartDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding = CartItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val cartItem = getItem(position)
        holder.bind(cartItem)
    }

    inner class CartViewHolder(private val binding: CartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(cartItem: CartEntity) {
            binding.apply {
                tvProductName.text = cartItem.productName
                tvProductVariant.text = cartItem.variantName

                val stockText = if (cartItem.stock!! < 10) {
                    "Sisa ${cartItem.stock}"
                } else {
                    "Stok ${cartItem.stock}"
                }
                tvProductStock.text = stockText

                if (cartItem.stock < 10) {
                    binding.tvProductStock.setTextColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.red
                        )
                    )
                } else {
                    binding.tvProductStock.setTextColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.black
                        )
                    )
                }

                tvQuantity.text = cartItem.quantity.toString()
                toggleButton.clearChecked()

                btnCheckBox.isChecked = cartItem.isSelected
                // Set an OnClickListener to handle CheckBox clicks
                btnCheckBox.setOnClickListener {
                    cartItem.isSelected = btnCheckBox.isChecked
                    listener.onCheckBoxClick(cartItem)
                }

                // Calculate the total price based on quantity
                val totalItemPrice = cartItem.productPrice!!.times(cartItem.quantity)
                tvProductPrice.text = formatPrice(totalItemPrice)

                Glide.with(itemView.context)
                    .load(cartItem.image)
                    .apply(
                        RequestOptions()
                            .placeholder(R.drawable.default_product_image)
                            .error(R.drawable.default_product_image)
                    )
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(ivProduct)

                // Handling addition button click
                btnAddition.setOnClickListener {
                    if (cartItem.stock > cartItem.quantity) {
                        cartItem.quantity += 1
                        listener.onAddButtonClick(cartItem)
                        notifyDataSetChanged()
                    }
                }

                // Handling subtraction button click
                btnSubtraction.setOnClickListener {
                    if (cartItem.quantity > 1) {
                        cartItem.quantity -= 1
                        listener.onSubtractButtonClick(cartItem)
                        notifyDataSetChanged()
                    }
                }

                // Handling delete item button click
                btnDelete.setOnClickListener {
                    listener.onDeleteButtonClick(cartItem)
                    notifyDataSetChanged()
                }
            }
        }
    }

    fun selectAllItems(isSelected: Boolean) {
        currentList.forEach { cartItem ->
            cartItem.isSelected = isSelected
        }
        notifyDataSetChanged()
    }

    fun getSelectedItems(): List<CartEntity> {
        return currentList.filter { it.isSelected }
    }


    private class CartDiffCallback : DiffUtil.ItemCallback<CartEntity>() {
        override fun areItemsTheSame(oldItem: CartEntity, newItem: CartEntity): Boolean {
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(oldItem: CartEntity, newItem: CartEntity): Boolean {
            return oldItem == newItem
        }
    }

    interface CartItemClickListener {
        fun onAddButtonClick(cartItem: CartEntity)
        fun onSubtractButtonClick(cartItem: CartEntity)
        fun onDeleteButtonClick(cartItem: CartEntity)
        fun onCheckBoxClick(cartItem: CartEntity)
    }
}