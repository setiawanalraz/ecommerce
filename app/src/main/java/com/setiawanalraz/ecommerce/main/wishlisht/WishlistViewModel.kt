package com.setiawanalraz.ecommerce.main.wishlisht

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.database.wishlist.WishlistEntity
import com.setiawanalraz.ecommerce.database.wishlist.WishlistRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(private val repository: WishlistRepository) :
    ViewModel() {

    val wishlistItems: LiveData<List<WishlistEntity>> = repository.wishlistItems

    fun deleteWishlistItem(item: WishlistEntity) {
        viewModelScope.launch {
            repository.deleteWishlistItem(item)
        }
    }

    fun addToWishlist(item: WishlistEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insertWishlistItem(item)
        }
    }

    fun removeFromWishlist(item: WishlistEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteWishlistItem(item)
        }
    }

    fun wishlistExist(productId: String): LiveData<Boolean> {
        return repository.wishlistExist(productId)
    }

    fun addToCart(cartItem: CartEntity) {
        viewModelScope.launch {
            repository.insertCartItem(cartItem)
        }
    }
}