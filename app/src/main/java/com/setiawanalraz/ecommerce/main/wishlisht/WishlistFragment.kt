package com.setiawanalraz.ecommerce.main.wishlisht

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.database.wishlist.WishlistEntity
import com.setiawanalraz.ecommerce.databinding.FragmentWishlistBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WishlistFragment : Fragment() {

    private var _binding: FragmentWishlistBinding? = null
    private val binding get() = _binding!!

    private val viewModel: WishlistViewModel by viewModels()
    private val wishlistAdapter by lazy {
        WishlistAdapter(
            onDeleteClickListener = { item -> onDeleteWishlistItem(item) },
            onAddClickListener = { item -> onAddToCartItem(item) }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()

        viewModel.wishlistItems.observe(viewLifecycleOwner) { wishlistItems ->
            wishlistAdapter.setWishlistItems(wishlistItems)

            binding.tvWishlistCount.text = resources.getQuantityString(
                R.plurals.wishlist_item_count,
                wishlistItems.size,
                wishlistItems.size
            )

            // Check if the wishlist is empty
            val wishlistItemCount = wishlistItems.size
            if (wishlistItemCount == 0) {
                binding.includedFragmentEmptyWishlist.fragmentEmptyWishlist.visibility =
                    View.VISIBLE
                binding.wishlistRecyclerView.visibility = View.GONE
            } else {
                binding.includedFragmentEmptyWishlist.fragmentEmptyWishlist.visibility = View.GONE
                binding.wishlistRecyclerView.visibility = View.VISIBLE
            }
        }
    }

    private fun setupRecyclerView() {
        binding.wishlistRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.wishlistRecyclerView.adapter = wishlistAdapter
    }

    private fun onDeleteWishlistItem(item: WishlistEntity) {
        viewModel.deleteWishlistItem(item)
    }

    private fun onAddToCartItem(item: WishlistEntity) {
        val cartItem = CartEntity(
            productId = item.productId,
            productName = item.productName,
            productPrice = item.productPrice,
            image = item.image,
            brand = item.brand,
            store = item.store,
            sale = item.sale,
            stock = item.stock,
            totalRating = item.totalRating,
            totalReview = item.totalReview,
            totalSatisfaction = item.totalSatisfaction,
            productRating = item.productRating,
            variantName = item.variantName,
            variantPrice = item.variantPrice ?: 0,
        )
        viewModel.addToCart(cartItem)
        Toast.makeText(requireContext(), "Dimasukkan dalam keranjang", Toast.LENGTH_SHORT).show()
    }
}