package com.setiawanalraz.ecommerce.main.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.setiawanalraz.ecommerce.MainActivity
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.data.ProductDetailData
import com.setiawanalraz.ecommerce.data.ProductVariant
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.database.wishlist.WishlistEntity
import com.setiawanalraz.ecommerce.databinding.FragmentProductDetailBinding
import com.setiawanalraz.ecommerce.helper.SharedPreferencesHelper
import com.setiawanalraz.ecommerce.helper.formatPrice
import com.setiawanalraz.ecommerce.main.wishlisht.WishlistViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

fun <T> LiveData<T>.observeOnce(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, object : Observer<T> {
        override fun onChanged(value: T) {
            observer(value)
            removeObserver(this)
        }
    })
}

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    private var _binding: FragmentProductDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ProductDetailViewModel by viewModels()
    private val wishlistViewModel: WishlistViewModel by viewModels()

    private var productDetail: ProductDetailData? = null
    private var selectedVariant: ProductVariant? = null

    private var totalPrice: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeData()
    }

    private fun initView() {
        binding.apply {
            topAppBar.setOnClickListener {
                findNavController().navigateUp()
            }

            btnSeeAllReview.setOnClickListener {
                val productId = arguments?.getString("productId")
                (requireActivity() as MainActivity).toReviewProduct(productId.toString())
            }

            btnAddToCart.setOnClickListener {
                if (productDetail != null && selectedVariant != null) {
                    val cartItem = CartEntity(
                        productId = arguments?.getString("productId").toString(),
                        productName = productDetail?.productName,
                        productPrice = totalPrice,
                        image = productDetail!!.image[0],
                        brand = productDetail!!.brand,
                        store = productDetail!!.store,
                        sale = productDetail!!.sale,
                        stock = productDetail!!.stock,
                        totalRating = productDetail!!.totalRating,
                        totalReview = productDetail!!.totalReview,
                        totalSatisfaction = productDetail!!.totalSatisfaction,
                        productRating = productDetail!!.productRating,
                        variantName = selectedVariant?.variantName,
                        variantPrice = selectedVariant?.variantPrice ?: 0,
                    )

                    viewLifecycleOwner.lifecycleScope.launch {
                        try {
                            viewModel.insertCartItem(cartItem)
                        } catch (e: Exception) {
                            Toast.makeText(
                                requireContext(),
                                e.message.toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    Toast.makeText(
                        requireContext(),
                        "Dimasukkan dalam keranjang",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Mohon pilih varian terlebih dahulu",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            icFavorite.setOnClickListener {
                if (productDetail != null) {
                    val productId = productDetail!!.productId
                    wishlistViewModel.wishlistExist(productId)
                        .observeOnce(viewLifecycleOwner) { isProductInWishlist ->
                            val wishlistItem = WishlistEntity(
                                productId = productId,
                                productName = productDetail!!.productName,
                                productPrice = productDetail!!.productPrice,
                                image = productDetail!!.image[0],
                                brand = productDetail!!.brand,
                                store = productDetail!!.store,
                                sale = productDetail!!.sale,
                                stock = productDetail!!.stock,
                                totalRating = productDetail!!.totalRating,
                                totalReview = productDetail!!.totalReview,
                                totalSatisfaction = productDetail!!.totalSatisfaction,
                                productRating = productDetail!!.productRating,
                                variantName = selectedVariant?.variantName,
                                variantPrice = selectedVariant?.variantPrice ?: 0,
                            )

                            if (isProductInWishlist) {
                                wishlistViewModel.removeFromWishlist(wishlistItem)
                                icFavorite.setImageResource(R.drawable.ic_favorite_border)
                                Toast.makeText(
                                    requireContext(),
                                    "Dihapus dalam daftar favorit",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                wishlistViewModel.addToWishlist(wishlistItem)
                                icFavorite.setImageResource(R.drawable.ic_favorite)
                                Toast.makeText(requireContext(), "Difavoritkan", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                }
            }
        }
    }

    private fun observeData() {
        val productId = arguments?.getString("productId")
        val authToken = sharedPreferencesHelper.getAccessToken().toString()

        viewModel.isLoading.value = true

        viewLifecycleOwner.lifecycleScope.launch {
            try {
                productDetail = viewModel.fetchProductDetail(authToken, productId.toString())
                viewModel.isLoading.value = false

                if (productDetail != null) {
                    setupProductViews()
                    observeWishlist(productId)
                } else {
                    binding.progressBar.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                Toast.makeText(requireContext(), e.message.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }
    }

    private fun observeWishlist(productId: String?) {
        if (productId != null) {
            wishlistViewModel.wishlistExist(productId)
                .observeOnce(viewLifecycleOwner) { isProductInWishlist ->
                    if (isProductInWishlist) {
                        binding.icFavorite.setImageResource(R.drawable.ic_favorite)
                    } else {
                        binding.icFavorite.setImageResource(R.drawable.ic_favorite_border)
                    }
                }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupProductViews() {
        binding.apply {
            val variantChipGroup = ChipGroup(requireContext())
            variantChipGroup.isSingleSelection = true

            for (index in productDetail!!.productVariant.indices) {
                val variant = productDetail!!.productVariant[index]
                val chip = Chip(requireContext())
                chip.text = variant.variantName
                chip.isCheckable = true

                if (index == 0) {
                    chip.isChecked = true
                    selectedVariant = variant
                    totalPrice = productDetail!!.productPrice + selectedVariant!!.variantPrice
                    updatePriceDisplay(totalPrice)
                }

                chip.setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked) {
                        selectedVariant = variant
                        totalPrice = productDetail!!.productPrice + selectedVariant!!.variantPrice
                    } else {
                        selectedVariant = null
                        totalPrice = productDetail!!.productPrice
                    }
                    updatePriceDisplay(totalPrice)
                }
                variantChipGroup.addView(chip)
            }

            val parentLayout = binding.cgVariant
            parentLayout.removeAllViews()
            parentLayout.addView(variantChipGroup)

            val formattedPrice = formatPrice(productDetail!!.productPrice)
            tvPrice.text = formattedPrice
            tvTotalReview.text = "(${productDetail!!.totalReview})"
            tvProductName.text = productDetail!!.productName
            tvSold.text = "Terjual ${productDetail!!.sale}"
            tvProductRating.text = productDetail!!.productRating.toString()
            tvDescriptionContent.text = productDetail!!.description
            tvProductRatingBottom.text = productDetail!!.productRating.toString()
            tvSatisfaction.text =
                "${productDetail!!.totalSatisfaction}% pembeli merasa puas"
            tvTotalRating.text = "${productDetail!!.totalRating} rating"
            tvTotalReviewBottom.text = "${productDetail!!.totalReview} ulasan"

            val imageUrl = productDetail!!.image[0]
            Glide.with(requireContext())
                .load(imageUrl)
                .placeholder(R.drawable.thumbnail_square)
                .error(R.drawable.thumbnail_square)
                .into(ivProduct)
        }
    }

    private fun updatePriceDisplay(price: Int) {
        val formattedPrice = formatPrice(price)
        binding.tvPrice.text = formattedPrice
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}