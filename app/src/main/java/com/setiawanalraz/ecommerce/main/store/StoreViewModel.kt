package com.setiawanalraz.ecommerce.main.store

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.setiawanalraz.ecommerce.data.Product
import com.setiawanalraz.ecommerce.network.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {

    fun getProducts(
        token: String,
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?,
    ): Flow<PagingData<Product>> {
        return Pager(
            config = PagingConfig(pageSize = 10, initialLoadSize = 10, prefetchDistance = 1),
            pagingSourceFactory = {
                ProductPagingSource(apiService, token, search, brand, lowest, highest, sort)
            }
        ).flow.cachedIn(viewModelScope)
    }
}