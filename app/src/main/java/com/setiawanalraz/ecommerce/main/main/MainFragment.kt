package com.setiawanalraz.ecommerce.main.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val navHostFragment: NavHostFragment by lazy {
        childFragmentManager.findFragmentById(R.id.nhf_child) as NavHostFragment
    }
    private val navController by lazy {
        navHostFragment.navController
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.bnvChild.setupWithNavController(navController)
        binding.bnvChild.setOnItemReselectedListener { }

        binding.topAppBar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.cart -> {
                    findNavController().navigate(R.id.main_to_cart)
                    true
                }

                else -> {
                    false
                }
            }
        }

        val cartMenuItem = binding.topAppBar.menu.findItem(R.id.cart)
        val badgeDrawable = BadgeDrawable.create(requireContext())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}