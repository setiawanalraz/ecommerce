package com.setiawanalraz.ecommerce.main.review

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.data.ProductReviewData

class ReviewAdapter(private val reviewDataList: List<ProductReviewData>?) :
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val userNameTextView: TextView = itemView.findViewById(R.id.tv_name)
        val userImageImageView: ImageView = itemView.findViewById(R.id.profile_image)
        val userRatingBar: RatingBar = itemView.findViewById(R.id.ratingBar)
        val userReviewTextView: TextView = itemView.findViewById(R.id.tv_review)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_review_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val reviewData = reviewDataList?.get(position)

        if (reviewData != null) {
            holder.userNameTextView.text = reviewData.userName
            holder.userRatingBar.rating = reviewData.userRating.toFloat()
            holder.userReviewTextView.text = reviewData.userReview

            Glide.with(holder.itemView)
                .load(reviewData.userImage)
                .placeholder(R.drawable.profile_badge)
                .error(R.drawable.profile_badge)
                .into(holder.userImageImageView)
        }
    }

    override fun getItemCount(): Int {
        return reviewDataList?.size ?: 0
    }
}
