package com.setiawanalraz.ecommerce.main.store

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.setiawanalraz.ecommerce.network.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

sealed class DataState<out T> {
    object Loading : DataState<Nothing>()
    data class Success<T>(val data: T) : DataState<T>()
    data class Error(val message: String) : DataState<Nothing>()
    object Reset : DataState<Nothing>()
}

@HiltViewModel
class SearchViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {

    private val _searchLiveData = MutableLiveData<DataState<Unit>>()
    val searchLiveData: LiveData<DataState<Unit>> = _searchLiveData

    fun searchProduct() {

    }

}