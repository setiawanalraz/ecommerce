package com.setiawanalraz.ecommerce.main.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.setiawanalraz.ecommerce.data.ProductReviewData
import com.setiawanalraz.ecommerce.data.ProductReviewResponse
import com.setiawanalraz.ecommerce.network.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {

    private val _productReview = MutableLiveData<ProductReviewResponse>()
    val productReview: LiveData<ProductReviewResponse> = _productReview

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    var isLoading = MutableLiveData<Boolean>()

    suspend fun fetchProductReview(authToken: String, productId: String): List<ProductReviewData>? {
        val response = apiService.getProductReview("Bearer $authToken", productId)
        if (response.isSuccessful) {
            return response.body()?.data
        }
        return null
    }
}