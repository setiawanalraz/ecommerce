package com.setiawanalraz.ecommerce.main.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.databinding.FragmentCartBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CartFragment : Fragment(), CartAdapter.CartItemClickListener {

    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CartViewModel by viewModels()
    private val cartAdapter = CartAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        observeData()

        binding.topAppBar.setOnClickListener {
            findNavController().navigateUp()
        }

        // Add an OnClickListener for the text button delete
        binding.tbtnDelete.setOnClickListener {
            val selectedItems = cartAdapter.getSelectedItems()
            viewModel.deleteSelectedItems(selectedItems)
        }

        // Check All
        binding.btnCheckAll.setOnClickListener {
            val isChecked = binding.btnCheckAll.isChecked
            cartAdapter.selectAllItems(isChecked)

            val selectedItems = cartAdapter.getSelectedItems()
            val totalPrice = viewModel.calculateTotalPrice(selectedItems)
            binding.tvTotalPrice.text = totalPrice

            val anyItemSelected = selectedItems.isNotEmpty()
            binding.tbtnDelete.visibility = if (anyItemSelected) View.VISIBLE else View.INVISIBLE

            binding.btnBuy.isEnabled = anyItemSelected
        }
    }

    private fun setupRecyclerView() {
        binding.cartRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.cartRecyclerView.adapter = cartAdapter
    }

    private fun observeData() {
        lifecycleScope.launch {
            viewModel.cartItems.collect { cartItems ->
                cartAdapter.submitList(cartItems)

                // Check is there any selected item
                val anyItemSelected = cartItems.any { it.isSelected }
                binding.tbtnDelete.visibility =
                    if (anyItemSelected) View.VISIBLE else View.INVISIBLE

                // Check all items
                val allItemSelected = cartItems.all { it.isSelected }
                binding.btnCheckAll.isChecked = allItemSelected

                // Total pay based on selected item
                val selectedItems = cartItems.filter { it.isSelected }
                val totalPrice = viewModel.calculateTotalPrice(selectedItems)
                binding.tvTotalPrice.text = totalPrice

                // Observe the totalPrice LiveData
                viewModel.totalPrice.observe(viewLifecycleOwner) { formattedPrice ->
                    binding.tvTotalPrice.text = formattedPrice
                }

                // Check if the cart is empty
                val cartItemCount = cartItems.size
                if (cartItemCount == 0) {
                    binding.includedFragmentEmptyCart.fragmentEmptyCart.visibility = View.VISIBLE
                    binding.cartRecyclerView.visibility = View.GONE
                } else {
                    binding.includedFragmentEmptyCart.fragmentEmptyCart.visibility = View.GONE
                    binding.cartRecyclerView.visibility = View.VISIBLE
                }

                binding.btnBuy.isEnabled = anyItemSelected or allItemSelected
            }
        }
    }

    override fun onAddButtonClick(cartItem: CartEntity) {
        viewModel.updateCartItem(cartItem)
    }

    override fun onSubtractButtonClick(cartItem: CartEntity) {
        viewModel.updateCartItem(cartItem)
    }

    override fun onDeleteButtonClick(cartItem: CartEntity) {
        viewModel.deleteCartItem(cartItem)
    }

    override fun onCheckBoxClick(cartItem: CartEntity) {
        viewModel.updateCartItemSelection(cartItem.productId, cartItem.isSelected)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}