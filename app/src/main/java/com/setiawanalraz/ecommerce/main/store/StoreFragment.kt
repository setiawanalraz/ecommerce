package com.setiawanalraz.ecommerce.main.store

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.facebook.shimmer.ShimmerFrameLayout
import com.setiawanalraz.ecommerce.MainActivity
import com.setiawanalraz.ecommerce.databinding.FragmentStoreBinding
import com.setiawanalraz.ecommerce.design.ModalBottomSheet
import com.setiawanalraz.ecommerce.design.SearchDialogFragment
import com.setiawanalraz.ecommerce.helper.SharedPreferencesHelper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class StoreFragment : Fragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    private var _binding: FragmentStoreBinding? = null
    private val binding get() = _binding!!

    private val viewModel: StoreViewModel by viewModels()

    private lateinit var shimmerLayout: ShimmerFrameLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val accessToken = sharedPreferencesHelper.getAccessToken()

        val productAdapter = ProductAdapter { productId ->
            navigateToProductDetail(productId)
        }

        binding.tietSearch.setOnClickListener {
            showDialog()
        }

        binding.filterChip.setOnClickListener {
            val modalBottomSheet = ModalBottomSheet()
            modalBottomSheet.show(childFragmentManager, modalBottomSheet.tag)
        }

        shimmerLayout = binding.shimmerLayoutList
        shimmerLayout.startShimmer()
        shimmerLayout.visibility = View.VISIBLE

        binding.productRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = productAdapter
        }

        viewLifecycleOwner.lifecycleScope.launch {
            try {
                viewModel.getProducts(
                    token = "Bearer $accessToken",
                    search = null,
                    brand = null,
                    lowest = null,
                    highest = null,
                    sort = null
                ).collectLatest { pagingData ->
                    productAdapter.submitData(pagingData)
                }
            } catch (e: IOException) {
                // Handle network errors.
                Log.d(e.toString(), "Network error occurred")
            } catch (e: Exception) {
                // Handle other exceptions.
                Log.d(e.toString(), "An error occurred")
            } finally {
                shimmerLayout.hideShimmer()
                shimmerLayout.stopShimmer()
            }
        }
    }

    private fun showDialog() {
        val fragmentManager = requireActivity().supportFragmentManager
        val newFragment = SearchDialogFragment()
        val transaction = fragmentManager.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction
            .add(android.R.id.content, newFragment)
            .addToBackStack(null)
            .commit()
    }

    private fun navigateToProductDetail(productId: String) {
        (requireActivity() as MainActivity).toDetailProduct(productId)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}