package com.setiawanalraz.ecommerce.main.store

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.setiawanalraz.ecommerce.data.Product
import com.setiawanalraz.ecommerce.network.ApiService
import retrofit2.HttpException

class ProductPagingSource(
    private val apiService: ApiService,
    private val token: String,
    private val search: String?,
    private val brand: String?,
    private val lowest: Int?,
    private val highest: Int?,
    private val sort: String?
) : PagingSource<Int, Product>() {

    override fun getRefreshKey(state: PagingState<Int, Product>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Product> {
        return try {
            val page = params.key ?: 1
            val limit = params.loadSize
            val response = apiService.products(
                token,
                search,
                brand,
                lowest,
                highest,
                sort,
                limit,
                page
            )

            if (response.isSuccessful) {
                val productsResponse = response.body()
                val products = productsResponse?.data?.items ?: emptyList()
                val nextPage =
                    if (page == (productsResponse?.data?.totalPages ?: 1)) null else page.plus(1)

                LoadResult.Page(
                    data = products,
                    prevKey = null,
                    nextKey = nextPage
                )
            } else {
                LoadResult.Error(HttpException(response))
            }
        } catch (e: Throwable) {
            LoadResult.Error(e)
        }
    }
}