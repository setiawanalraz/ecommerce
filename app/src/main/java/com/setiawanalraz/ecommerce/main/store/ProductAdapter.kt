package com.setiawanalraz.ecommerce.main.store

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.data.Product
import com.setiawanalraz.ecommerce.databinding.ItemProductListBinding
import com.setiawanalraz.ecommerce.helper.formatPrice

class ProductAdapter(private val onItemClick: (productId: String) -> Unit) :
    PagingDataAdapter<Product, ProductAdapter.ProductViewHolder>(ProductDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding =
            ItemProductListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        getItem(position)?.let { product ->
            holder.bind(product)
            holder.itemView.setOnClickListener {
                onItemClick(product.productId)
            }
        }
    }

    class ProductViewHolder(private val binding: ItemProductListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(product: Product) {
            binding.apply {
                tvProductName.text = product.productName

                val formattedPrice = formatPrice(product.productPrice)
                tvProductPrice.text = formattedPrice

                tvStoreName.text = product.store
                tvProductRating.text = product.productRating.toString()

                val saleText = itemView.context.getString(R.string.total_sale)
                tvSale.text = "${product.sale} $saleText"

                Glide.with(itemView.context)
                    .load(product.image)
                    .apply(
                        RequestOptions()
                            .placeholder(R.drawable.default_product_image)
                            .error(R.drawable.default_product_image)
                    )
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(ivProduct)
            }
        }
    }

    class ProductDiffCallback : DiffUtil.ItemCallback<Product>() {
        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }
    }
}