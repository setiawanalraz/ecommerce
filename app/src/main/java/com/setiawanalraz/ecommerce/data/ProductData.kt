package com.setiawanalraz.ecommerce.data

data class ProductsResponse(
    val code: Int,
    val message: String,
    val data: ProductData
)

data class ProductData(
    val itemsPerPage: Int,
    val currentItemCount: Int,
    val pageIndex: Int,
    val totalPages: Int,
    val items: List<Product>
)

data class Product(
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val store: String,
    val sale: Int,
    val productRating: Float
)

data class SearchRequest(val query: String)

data class SearchResponse(val code: Int, val message: String, val data: List<String>)

data class ProductDetailResponse(
    val code: Int,
    val message: String,
    val data: ProductDetailData
)

data class ProductDetailData(
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: List<String>,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Float,
    val productVariant: List<ProductVariant>
)

data class ProductVariant(
    val variantName: String,
    val variantPrice: Int
)

data class ProductReviewResponse(
    val code: Int,
    val message: String,
    val data: List<ProductReviewData>
)

data class ProductReviewData(
    val userName: String,
    val userImage: String,
    val userRating: Int,
    val userReview: String
)