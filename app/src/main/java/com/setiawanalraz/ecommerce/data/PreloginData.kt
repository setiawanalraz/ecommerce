package com.setiawanalraz.ecommerce.data

import okhttp3.MultipartBody

data class RegistrationRequest(
    val email: String,
    val password: String,
    val firebaseToken: String
)

data class RegistrationResponse(
    val code: Int,
    val message: String,
    val data: RegistrationData
)

data class RegistrationData(
    val accessToken: String,
    val refreshToken: String,
    val expiresAt: Long
)

data class ErrorResponse(
    val code: Int,
    val message: String
)

data class LoginRequest(
    val email: String,
    val password: String,
    val firebaseToken: String
)

data class LoginResponse(
    val code: Int,
    val message: String,
    val data: LoginData
)

data class LoginData(
    val userName: String,
    val userImage: String,
    val accessToken: String,
    val refreshToken: String,
    val expiresAt: Long
)

data class RefreshRequest(
    val token: String
)

data class RefreshResponse(
    val accessToken: String,
    val refreshToken: String,
    val expiresAt: Long
)

data class ProfileResponse(
    val code: Int,
    val message: String,
    val data: ProfileData
)

data class ProfileData(
    val userName: String,
    val userImage: MultipartBody.Part?
)