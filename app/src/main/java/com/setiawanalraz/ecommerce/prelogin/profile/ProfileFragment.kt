package com.setiawanalraz.ecommerce.prelogin.profile

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.color.MaterialColors
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.databinding.FragmentProfileBinding
import com.setiawanalraz.ecommerce.helper.setSubstringColor
import java.io.File

class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

//    private lateinit var viewModel: ProfileViewModel

    private var uri: Uri? = null

    private val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { success ->
            if (success) {
                val imageFile = File(requireContext().cacheDir, generateFilename())
                Glide.with(requireContext())
                    .load(uri)
                    .into(binding.profileImage)

                binding.icPerson.visibility = View.GONE
            } else {
                Log.d("PhotoPicker", "No media selected")
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
//        viewModel = ViewModelProvider(requireActivity())[ProfileViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
//        observeData()
    }

    private fun initView() {
        binding.apply {
            val items = arrayOf("Kamera", "Galeri")

            profileImage.setOnClickListener {
                context?.let {
                    MaterialAlertDialogBuilder(it)
                        .setTitle(resources.getString(R.string.select_image_dialog))
                        .setItems(items) { _, which ->
                            when (which) {
                                0 -> {
                                    uri = buildNewUri()
                                    takePicture.launch(uri)
                                }

                                1 -> {
                                    pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
                                }
                            }
                        }
                        .show()
                }
            }

            btnDone.setOnClickListener {
                findNavController().navigate(R.id.prelogin_to_main)
//                updateProfile()
            }

            val spannable =
                tvRegisterProfileTermsAndConditions.text.toString().setSubstringColor(
                    listOf(getString(R.string.text_terms), getString(R.string.text_conditions)),
                    MaterialColors.getColor(
                        requireView(),
                        com.google.android.material.R.attr.colorPrimary
                    )
                )
            tvRegisterProfileTermsAndConditions.text = spannable
        }
    }

//    private fun updateProfile() {
//        val authorization = "Bearer YOUR_AUTH_TOKEN" // Replace with your actual auth token
//        val userName = RequestBody.create(MultipartBody.FORM, "YourUsername")
//        val imageFile = File("path_to_image_file")
//        val imagePart = MultipartBody.Part.createFormData("userImage", imageFile.name, imageFile.asRequestBody("image/*".toMediaTypeOrNull()))
//
//        viewModel.profileData(authorization, imagePart, userName)
//    }

//    private fun observeData() {
//        viewModel.profileLiveData.observe(viewLifecycleOwner) { dataState ->
//            when (dataState) {
//                is DataState.Loading -> {
//                    // Handle loading state, if needed
//                }
//
//                is DataState.Success<*> -> {
//                    // Handle success state
//                    showToast("Register successful")
//                    findNavController().navigate(R.id.action_profileFragment_to_mainFragment2)
//                }
//
//                is DataState.Error -> {
//                    // Handle error state
//                    showToast("Register failed: ${dataState.message}")
//                }
//            }
//        }
//    }

    private val pickMedia =
        this.registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            if (uri != null) {
                binding.profileImage.setImageURI(uri)
                binding.icPerson.visibility = View.GONE
            } else {
                Log.d("PhotoPicker", "No media selected")
            }
        }

    private fun buildNewUri(): Uri {
        val photosDir = File(requireContext().cacheDir, PHOTOS_DIR)
        photosDir.mkdirs()
        val photoFile = File(photosDir, generateFilename())
        val authority = "${requireContext().packageName}.$FILE_PROVIDER"
        return FileProvider.getUriForFile(requireContext(), authority, photoFile)
    }

    private fun generateFilename() = "selfie-${System.currentTimeMillis()}.jpg"

    companion object {
        private const val PHOTOS_DIR = "photos"
        private const val FILE_PROVIDER = "fileprovider"
    }

//    private fun showToast(message: String) {
//        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
//    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}