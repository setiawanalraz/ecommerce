package com.setiawanalraz.ecommerce.prelogin.profile
//
//import androidx.lifecycle.LiveData
//import androidx.lifecycle.MutableLiveData
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.viewModelScope
//import com.setiawanalraz.ecommerce.network.ApiService
//import com.setiawanalraz.ecommerce.network.ProfileData
//import dagger.hilt.android.lifecycle.HiltViewModel
//import kotlinx.coroutines.launch
//import okhttp3.MultipartBody
//import okhttp3.RequestBody
//import retrofit2.HttpException
//import java.io.IOException
//import javax.inject.Inject
//
//sealed class DataState<out T> {
//    object Loading : DataState<Nothing>()
//    data class Success<T>(val data: T)
//    data class Error(val message: String) : DataState<Nothing>()
//}
//
//@HiltViewModel
//class ProfileViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {
//    private val _profileLiveData = MutableLiveData<DataState<Unit>>()
//    val profileLiveData: LiveData<DataState<Unit>> = _profileLiveData
//
//    fun profileData(userImage: String, userName: MultipartBody.Part, userName1: RequestBody) {
//        val profileRequest = ProfileData(userImage, userName)
//
//        viewModelScope.launch {
//            try {
//                val response = apiService.profile(profileRequest)
//                if (response.isSuccessful) {
//                    _profileLiveData.postValue(DataState.Success(Unit))
//                } else {
//                    _profileLiveData.postValue(DataState.Error(response.message().toString()))
//                }
//
//            } catch (e: HttpException) {
//                _profileLiveData.postValue(DataState.Error("HTTP error: ${e.message()}"))
//            } catch (e: IOException) {
//                _profileLiveData.postValue(DataState.Error("Network error: ${e.message}"))
//            } catch (e: Exception) {
//                _profileLiveData.postValue(DataState.Error(e.message.toString()))
//            }
//        }
//
//    }
//
//}

//sealed class DataState<out T> {
//    object Loading : DataState<Nothing>()
//    data class Success<T>(val data: T)
//    data class Error(val message: String) : DataState<Nothing>()
//}
//
//@HiltViewModel
//class ProfileViewModel @Inject constructor(
//    private val apiService: ApiService,
//    private val sharedPreferencesHelper: SharedPreferencesHelper
//) : ViewModel() {
//    private val _profileLiveData = MutableLiveData<DataState<Unit>>()
//    val profileLiveData: LiveData<DataState<Unit>> = _profileLiveData
//
//
//    fun profileUser(userName: String, userImage: String) {
//        val profileRequest = profileUser(userName, userImage)
//        viewModelScope.launch {
//            try {
//                val response = apiService.profile(profileRequest)
//                if (response.isSuccessful) {
//                    // Save the accessToken when registration is successful
//                    val apiResponse = response.body()
//                    if (apiResponse != null) {
//
//                    }
////                    _profileLiveData.postValue(DataState.Success(Unit))
////                    _profileLiveData.postValue(DataState<Unit>!)
//
//                } else {
//                    _profileLiveData.postValue(DataState.Error(response.message().toString()))
//                }
//
//            } catch (e: HttpException) {
//                _profileLiveData.postValue(DataState.Error("HTTP error: ${e.message()}"))
//            } catch (e: IOException) {
//                _profileLiveData.postValue(DataState.Error("Network error: ${e.message}"))
//            } catch (e: Exception) {
//                _profileLiveData.postValue(DataState.Error(e.message.toString()))
//            }
//        }
//    }
//
//}