package com.setiawanalraz.ecommerce.prelogin.onboarding

data class IntroSlide(
    val icon: Int
)