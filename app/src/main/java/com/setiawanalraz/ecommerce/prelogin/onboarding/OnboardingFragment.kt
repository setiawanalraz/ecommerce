package com.setiawanalraz.ecommerce.prelogin.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.databinding.FragmentOnboardingBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingFragment : Fragment() {

    private var _binding: FragmentOnboardingBinding? = null
    private val binding get() = _binding!!

    private val viewModel: OnboardingViewModel by viewModels()

    private val introSliderAdapter = IntroSliderAdapter(
        listOf(
            IntroSlide(
                R.drawable.onboarding_image1
            ),
            IntroSlide(
                R.drawable.onboarding_image2
            ),
            IntroSlide(
                R.drawable.onboarding_image3
            )
        )
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOnboardingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val isFirstTimeLaunch = viewModel.isFirstTimeLaunch()

        val introSliderViewPager = binding.introSliderViewPager
        introSliderViewPager.adapter = introSliderAdapter

        setupIndicators()
        setCurrentIndicator(0)
        introSliderViewPager.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentIndicator(position)
            }
        })

        if (!isFirstTimeLaunch) {
            findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
        }
        binding.btnRegister.setOnClickListener {
            viewModel.setFirstTimeLaunch(false)
            findNavController().navigate(R.id.action_onboardingFragment_to_registerFragment)
        }

        binding.btnSkip.setOnClickListener {
            viewModel.setFirstTimeLaunch(false)
            findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
        }

        binding.btnNext.setOnClickListener {
            introSliderViewPager.currentItem++
        }
    }

    private fun setupIndicators() {
        val indicatorsContainer = binding.indicatorContainer
        val indicators = arrayOfNulls<ImageView>(introSliderAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(requireContext())
            indicators[i].apply {
                this?.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.indicator_inactive
                    )
                )
                this?.layoutParams = layoutParams
            }
            indicatorsContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicator(index: Int) {
        val indicatorsContainer = binding.indicatorContainer
        val childCount = indicatorsContainer.childCount
        for (i in 0 until childCount) {
            val imageView = indicatorsContainer[i] as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.indicator_active
                    )
                )
                if (i == childCount - 1) {
                    binding.btnNext.visibility = View.GONE
                } else {
                    binding.btnNext.visibility = View.VISIBLE
                }

            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.indicator_inactive
                    )
                )
            }
        }
    }
}