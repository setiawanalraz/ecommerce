package com.setiawanalraz.ecommerce.prelogin.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.setiawanalraz.ecommerce.data.LoginRequest
import com.setiawanalraz.ecommerce.helper.SharedPreferencesHelper
import com.setiawanalraz.ecommerce.network.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

sealed class DataState<out T> {
    object Loading : DataState<Nothing>()
    data class Success<T>(val data: T) : DataState<T>()
    data class Error(val message: String) : DataState<Nothing>()
    object Reset : DataState<Nothing>()
}

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val apiService: ApiService,
    private val sharedPreferencesHelper: SharedPreferencesHelper
) : ViewModel() {

    private val _loginLiveData = MutableLiveData<DataState<Unit>>()
    val loginLiveData: LiveData<DataState<Unit>> = _loginLiveData

    fun loginUser(email: String, password: String, firebaseToken: String) {
        val loginRequest = LoginRequest(email, password, firebaseToken)
        viewModelScope.launch {
            try {
                val response = apiService.login(loginRequest)
                if (response.isSuccessful) {
                    // Save the accessToken when registration is successful
                    val apiResponse = response.body()
                    if (apiResponse != null) {
                        val accessToken = apiResponse.data.accessToken
                        sharedPreferencesHelper.saveAccessToken(accessToken)
                        Log.d("AccessToken", accessToken)
                    }
                    _loginLiveData.postValue(DataState.Success(Unit))
                } else {
                    _loginLiveData.postValue(DataState.Error("${response.code()} (${response.message()})"))
                }

            } catch (e: HttpException) {
                _loginLiveData.postValue(DataState.Error("HTTP error: ${e.message()}"))
            } catch (e: IOException) {
                _loginLiveData.postValue(DataState.Error("Network error: ${e.message}"))
            } catch (e: Exception) {
                _loginLiveData.postValue(DataState.Error(e.message.toString()))
            }
        }
    }

    fun reset() {
        _loginLiveData.postValue(DataState.Reset)
    }
}