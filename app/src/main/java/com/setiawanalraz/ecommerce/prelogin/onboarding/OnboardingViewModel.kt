package com.setiawanalraz.ecommerce.prelogin.onboarding

import androidx.lifecycle.ViewModel
import com.setiawanalraz.ecommerce.helper.SharedPreferencesHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OnboardingViewModel @Inject constructor(
    private val sharedPreferencesHelper: SharedPreferencesHelper
) : ViewModel() {

    fun isFirstTimeLaunch(): Boolean {
        return sharedPreferencesHelper.isFirstTimeLaunch()
    }

    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        sharedPreferencesHelper.setFirstTimeLaunch(isFirstTime)
    }
}
