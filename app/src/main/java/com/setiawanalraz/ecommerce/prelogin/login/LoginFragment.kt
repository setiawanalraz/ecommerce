package com.setiawanalraz.ecommerce.prelogin.login

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.util.PatternsCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.color.MaterialColors
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.databinding.FragmentLoginBinding
import com.setiawanalraz.ecommerce.helper.setSubstringColor

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private var isEmailValid = false
    private var isPasswordValid = false
    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(requireActivity())[LoginViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeData()
    }

    private fun initView() {
        binding.apply {
            val registerButton = btnRegister
            val loginButton = btnLogin

            registerButton.setOnClickListener {
                findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }

            loginButton.setOnClickListener {
                binding.circularProgressBar.visibility = View.VISIBLE
                performLogin()
            }

            // Get input email
            filledEmailLayout.helperText = "Contoh: test@gmail.com"
            filledEmailLayout.editText?.doOnTextChanged { inputEmail, _, _, _ ->
                if (inputEmail.isNullOrBlank()) {
                    filledEmailLayout.helperText = "Contoh: test@gmail.com"
                    isEmailValid = false
                } else if (inputEmail.isNotBlank() && !PatternsCompat.EMAIL_ADDRESS.matcher(
                        inputEmail
                    )
                        .matches()
                ) {
                    filledEmailLayout.error = "Email tidak valid"
                    isEmailValid = false
                } else {
                    filledEmailLayout.error = null
                    isEmailValid = true
                }
                updateLoginButtonState(loginButton)
            }

            // Get input password
            filledPasswordLayout.helperText = "Minimal 8 karakter"
            filledPasswordLayout.editText?.doOnTextChanged { inputPassword, _, _, _ ->
                if (inputPassword.isNullOrBlank()) {
                    filledPasswordLayout.helperText = "Minimal 8 karakter"
                    isPasswordValid = false
                } else if (inputPassword.length < 8) {
                    filledPasswordLayout.error = "Password tidak valid"
                    isPasswordValid = false
                } else {
                    filledPasswordLayout.error = null
                    isPasswordValid = true
                }
                updateLoginButtonState(loginButton)
            }

            // Spannable Text
            val spannable = tvLoginTermsAndConditions.text.toString().setSubstringColor(
                listOf(getString(R.string.text_terms), getString(R.string.text_conditions)),
                MaterialColors.getColor(
                    requireView(),
                    com.google.android.material.R.attr.colorPrimary
                )
            )
            tvLoginTermsAndConditions.text = spannable
        }
    }

    private fun performLogin() {
        val email = binding.edtEmail.text.toString()
        val password = binding.edtPassword.text.toString()
        val firebaseToken = ""

        viewModel.loginUser(email, password, firebaseToken)
    }

    private fun updateLoginButtonState(loginButton: Button) {
        loginButton.isEnabled = isEmailValid && isPasswordValid
    }

    private fun observeData() {
        // Observe loginResponse LiveData
        viewModel.loginLiveData.observe(viewLifecycleOwner) { dataState ->
            when (dataState) {
                is DataState.Loading -> {
                    // Handle loading state, if needed
                    d("STATE", "DataState Loading")
                    binding.circularProgressBar.visibility = View.VISIBLE
                }

                is DataState.Success -> {
                    // Handle success state
                    d("STATE", "DataState Success")
                    binding.circularProgressBar.visibility = View.INVISIBLE
                    showToast("Login successful")
                    findNavController().navigate(R.id.prelogin_to_main)
                    viewModel.reset()
                }

                is DataState.Error -> {
                    // Handle error state
                    d("STATE", "DataState Error")
                    binding.circularProgressBar.visibility = View.INVISIBLE
                    showToast("Login failed: ${dataState.message}")
                }

                else -> {}
            }
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}