package com.setiawanalraz.ecommerce.prelogin.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.util.PatternsCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.color.MaterialColors
import com.setiawanalraz.ecommerce.R
import com.setiawanalraz.ecommerce.databinding.FragmentRegisterBinding
import com.setiawanalraz.ecommerce.helper.setSubstringColor

class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var isEmailValid = false
    private var isPasswordValid = false

    //        private val viewModel: RegisterViewModel by viewModels() //result error: cannot create an instance class
    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(requireActivity())[RegisterViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeData()
    }

    private fun initView() {
        binding.apply {
            val registerButton = btnRegister
            val loginButton = btnLogin

            registerButton.setOnClickListener {
                binding.circularProgressBar.visibility = View.VISIBLE
                performRegistration()
            }

            loginButton.setOnClickListener {
                findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
            }

            filledEmailLayout.helperText = "Contoh: test@gmail.com"
            filledEmailLayout.editText?.doOnTextChanged { inputEmail, _, _, _ ->
                if (inputEmail.isNullOrBlank()) {
                    filledEmailLayout.helperText = "Contoh: test@gmail.com"
                    isEmailValid = false
                } else if (inputEmail.isNotBlank() && !PatternsCompat.EMAIL_ADDRESS.matcher(
                        inputEmail
                    ).matches()
                ) {
                    filledEmailLayout.error = "Email tidak valid"
                    isEmailValid = false
                } else {
                    filledEmailLayout.error = null
                    isEmailValid = true
                }
                updateRegisterButtonState(registerButton)
            }

            filledPasswordLayout.helperText = "Minimal 8 karakter"
            filledPasswordLayout.editText?.doOnTextChanged { inputPassword, _, _, _ ->
                if (inputPassword.isNullOrBlank()) {
                    filledPasswordLayout.helperText = "Minimal 8 karakter"
                    isPasswordValid = false
                } else if (inputPassword.length < 8) {
                    filledPasswordLayout.error = "Password tidak valid"
                    isPasswordValid = false
                } else {
                    filledPasswordLayout.error = null
                    isPasswordValid = true
                }
                updateRegisterButtonState(registerButton)
            }

            val spannable = tvRegisterTermsAndConditions.text.toString().setSubstringColor(
                listOf(getString(R.string.text_terms), getString(R.string.text_conditions)),
                MaterialColors.getColor(
                    requireView(),
                    com.google.android.material.R.attr.colorPrimary
                )
            )
            tvRegisterTermsAndConditions.text = spannable
        }
    }

    private fun performRegistration() {
        val email = binding.edtEmail.text.toString()
        val password = binding.edtPassword.text.toString()
        val firebaseToken = ""

        viewModel.registerUser(email, password, firebaseToken)
    }

    private fun updateRegisterButtonState(registerButton: Button) {
        registerButton.isEnabled = isEmailValid && isPasswordValid
    }

    private fun observeData() {
        viewModel.registrationLiveData.observe(viewLifecycleOwner) { dataState ->
            when (dataState) {
                is DataState.Loading -> {
                    binding.circularProgressBar.visibility = View.VISIBLE
                }

                is DataState.Success -> {
                    binding.circularProgressBar.visibility = View.INVISIBLE
                    showToast("Registration successful")
                    findNavController().navigate(R.id.action_registerFragment_to_profileFragment)
                }

                is DataState.Error -> {
                    binding.circularProgressBar.visibility = View.INVISIBLE
                    showToast("Registration failed: ${dataState.message}")
                }
            }
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}