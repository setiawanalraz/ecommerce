package com.setiawanalraz.ecommerce.prelogin.register

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.setiawanalraz.ecommerce.data.RegistrationRequest
import com.setiawanalraz.ecommerce.helper.SharedPreferencesHelper
import com.setiawanalraz.ecommerce.network.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

sealed class DataState<out T> {
    object Loading : DataState<Nothing>()
    data class Success<T>(val data: T) : DataState<T>()
    data class Error(val message: String) : DataState<Nothing>()
}

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val apiService: ApiService,
    private val sharedPreferencesHelper: SharedPreferencesHelper
) : ViewModel() {

    private val _registrationLiveData = MutableLiveData<DataState<Unit>>()
    val registrationLiveData: LiveData<DataState<Unit>> = _registrationLiveData

    fun registerUser(email: String, password: String, firebaseToken: String) {
        val registrationRequest = RegistrationRequest(email, password, firebaseToken)
        viewModelScope.launch {
            try {
                val response = apiService.register(registrationRequest)
                if (response.isSuccessful) {
                    // Save the accessToken when registration is successful
                    val apiResponse = response.body()
                    if (apiResponse != null) {
                        val accessToken = apiResponse.data.accessToken
                        sharedPreferencesHelper.saveAccessToken(accessToken)

                        Log.d("AccessToken", accessToken)
                    }
                    _registrationLiveData.postValue(DataState.Success(Unit))
                } else {
                    _registrationLiveData.postValue(DataState.Error("${response.code()} (${response.message()})"))
                }
            } catch (e: HttpException) {
                _registrationLiveData.postValue(DataState.Error("HTTP error: ${e.message()}"))
            } catch (e: IOException) {
                _registrationLiveData.postValue(DataState.Error("Network error: ${e.message}"))
            } catch (e: Exception) {
                _registrationLiveData.postValue(DataState.Error(e.message.toString()))
            }
        }
    }
}