package com.setiawanalraz.ecommerce

import android.app.Application
import androidx.room.Room
import com.setiawanalraz.ecommerce.database.AppDatabase
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application() {
    val database by lazy {
        Room.databaseBuilder(this, AppDatabase::class.java, "e-commerce")
            .fallbackToDestructiveMigration().build()
    }
}