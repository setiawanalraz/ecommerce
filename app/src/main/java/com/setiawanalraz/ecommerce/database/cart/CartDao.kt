package com.setiawanalraz.ecommerce.database.cart

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface CartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCart(vararg cartEntity: CartEntity)

    @Update
    suspend fun updateCart(vararg cartEntity: CartEntity)

    @Delete
    suspend fun deleteCart(vararg cartEntity: CartEntity)

    @Query("SELECT * FROM CartEntity")
    fun getAllCartItems(): Flow<List<CartEntity>>

    @Query("SELECT COUNT(*) FROM CartEntity")
    fun getCartItemCount(): Int

    @Query("SELECT * FROM CartEntity WHERE productId = :id")
    fun loadCartById(id: String): LiveData<CartEntity>

    @Query("UPDATE CartEntity SET isSelected = :isSelected WHERE productId = :id")
    fun updateCartItemSelection(id: String, isSelected: Boolean)

    @Query("DELETE FROM CartEntity")
    suspend fun deleteAllCartItems()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateCartItems(cartItems: List<CartEntity>)

    @Delete
    suspend fun deleteCartItems(cartItems: List<CartEntity>)
}