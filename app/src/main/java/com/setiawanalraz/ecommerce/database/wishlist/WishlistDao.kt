package com.setiawanalraz.ecommerce.database.wishlist

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface WishlistDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWishlist(wishlistEntity: WishlistEntity)

    @Delete
    suspend fun deleteWishlist(wishlistEntity: WishlistEntity)

    @Query("SELECT * FROM WishlistEntity")
    fun getWishlist(): LiveData<List<WishlistEntity>>

    @Query("SELECT COUNT(*) FROM WishlistEntity")
    fun getWishlistCount(): LiveData<Int>

    @Query("SELECT EXISTS (SELECT 1 FROM WishlistEntity WHERE productId = :productId)")
    fun wishlistExist(productId: String): LiveData<Boolean>
}