package com.setiawanalraz.ecommerce.database.wishlist

import androidx.lifecycle.LiveData
import com.setiawanalraz.ecommerce.database.cart.CartDao
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import javax.inject.Inject

class WishlistRepository @Inject constructor(
    private val wishlistDao: WishlistDao,
    private val cartDao: CartDao
) {

    val wishlistItems: LiveData<List<WishlistEntity>> = wishlistDao.getWishlist()

    suspend fun insertWishlistItem(item: WishlistEntity) {
        wishlistDao.insertWishlist(item)
    }

    suspend fun deleteWishlistItem(item: WishlistEntity) {
        wishlistDao.deleteWishlist(item)
    }

    fun wishlistExist(productId: String): LiveData<Boolean> {
        return wishlistDao.wishlistExist(productId)
    }

    suspend fun insertCartItem(item: CartEntity) {
        return cartDao.insertCart(item)
    }
}
