package com.setiawanalraz.ecommerce.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.setiawanalraz.ecommerce.database.cart.CartDao
import com.setiawanalraz.ecommerce.database.cart.CartEntity
import com.setiawanalraz.ecommerce.database.wishlist.WishlistDao
import com.setiawanalraz.ecommerce.database.wishlist.WishlistEntity

@Database(entities = [CartEntity::class, WishlistEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun cartDao(): CartDao
    abstract fun wishlistDao(): WishlistDao

    companion object {
        private const val DATABASE_NAME = "e-commerce"

        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}