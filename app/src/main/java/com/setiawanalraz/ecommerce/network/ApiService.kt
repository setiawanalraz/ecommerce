package com.setiawanalraz.ecommerce.network

import com.setiawanalraz.ecommerce.data.LoginRequest
import com.setiawanalraz.ecommerce.data.LoginResponse
import com.setiawanalraz.ecommerce.data.ProductDetailResponse
import com.setiawanalraz.ecommerce.data.ProductReviewResponse
import com.setiawanalraz.ecommerce.data.ProductsResponse
import com.setiawanalraz.ecommerce.data.ProfileResponse
import com.setiawanalraz.ecommerce.data.RefreshRequest
import com.setiawanalraz.ecommerce.data.RefreshResponse
import com.setiawanalraz.ecommerce.data.RegistrationRequest
import com.setiawanalraz.ecommerce.data.RegistrationResponse
import com.setiawanalraz.ecommerce.data.SearchResponse
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @POST("register")
    @Headers("API_KEY: 6f8856ed-9189-488f-9011-0ff4b6c08edc")
    suspend fun register(
        @Body registrationRequest: RegistrationRequest
    ): Response<RegistrationResponse>

    @POST("login")
    @Headers("API_KEY: 6f8856ed-9189-488f-9011-0ff4b6c08edc")
    suspend fun login(
        @Body loginRequest: LoginRequest
    ): Response<LoginResponse>

    @POST("refresh")
    @Headers("API_KEY: 6f8856ed-9189-488f-9011-0ff4b6c08edc")
    suspend fun refresh(
        @Body refreshRequest: RefreshRequest
    ): Response<RefreshResponse>

    @Multipart
    @POST("profile")
    suspend fun profile(
        @Header("Authorization") authorization: String,
        @Part("userName") userName: String,
        @Part userImage: MultipartBody.Part?
    ): Response<ProfileResponse>

    @POST("products")
    suspend fun products(
        @Header("Authorization") token: String,
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("lowest") lowest: Int?,
        @Query("highest") highest: Int?,
        @Query("sort") sort: String?,
        @Query("limit") limit: Int?,
        @Query("page") page: Int?
    ): Response<ProductsResponse>

    // test get all product list
    @POST("products")
    suspend fun allProducts(
        @Header("Authorization") token: String,
    ): Response<ProductsResponse>

    @POST("search")
    suspend fun search(
        @Header("Authorization") authorization: String,
        @Query("query") query: String?
    ): Response<SearchResponse>

    @GET("products/{id}")
    suspend fun getProductDetail(
        @Header("Authorization") authorization: String,
        @Path("id") productId: String
    ): Response<ProductDetailResponse>

    @GET("review/{id}")
    suspend fun getProductReview(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Response<ProductReviewResponse>
}